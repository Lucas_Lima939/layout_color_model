Essa classe tem como métodos as cores de fundo, cores primárias e secundárias, o mesmo método pode ser replicado
para cores de textos etc.
A criação das cores é feita no MaterialColor, é super importante que seja construído assim pois é a forma mais 
rápida de prática de chamar, essas cores podem ser chamadas em qualquer widget que tenha o atributo "color".
A criação de cores no MaterialColor é feita através de códigos hexadécimais das cores, a base para alteração de
cores no construtor é: 0xff......
Ao identificar a cor desejada, lendo em algum programa específico, recomendo o site da Adobe, o código da cor
pode ser colocado no espaço vazio.
O construtor é bem completo e recebe também o código RGB da cor em questão e pode fazer as tonalidades, sendo
900 o mais forte e 50 o mais fraco.
O código é bem improvisado para ser funcional, pode e deve ser melhor polido e acabado.

Para chamar a classe e o métodos em widgets:

~importar a classe~

theme: ThemeData(
        primarySwatch: LayoutColor.secondaryColor);
		
Text('teste', style: TextStyle(color: LayoutColor.primaryColor));